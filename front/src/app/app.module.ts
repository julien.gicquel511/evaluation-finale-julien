import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './component/header/header.component';
import { FooterComponent } from './component/footer/footer.component';
import { ProjectComponent } from './component/project/project.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateProjectComponent } from './pages/create-project/create-project.component';
import { FieldComponent } from './component/ui/field/field.component';
import { LucideAngularModule, File, Home, Menu, UserCheck } from 'lucide-angular';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { CreateTicketComponent } from './pages/create-ticket/create-ticket.component';
import { FieldModule } from './component/ui/field/field.module';
import { ButtonModule } from './component/ui/button/button.module';
import { UpdateTicketComponent } from './pages/update-ticket/update-ticket.component';
import { UpdateProjectComponent } from './pages/update-project/update-project.component';
import { DetailTicketComponent } from './pages/detail-ticket/detail-ticket.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ProjectComponent,
    CreateProjectComponent,
    LoginComponent,
    HomeComponent,
    CreateTicketComponent,
    UpdateTicketComponent,
    UpdateProjectComponent,
    DetailTicketComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    LucideAngularModule.pick({File, Home, Menu, UserCheck}),
    ReactiveFormsModule,
    FieldModule,
    ButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
