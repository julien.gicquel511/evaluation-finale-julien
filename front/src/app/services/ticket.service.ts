import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Project } from '../model/project';
import { Observable } from 'rxjs';
import { Ticket } from '../model/ticket';

@Injectable({
  providedIn: 'root'
})
export class TicketService {
  constructor(private httpClient: HttpClient) { }

  getTicketsByProjectId(idProject: number): Observable<Ticket[]>{
    return this.httpClient.get<Ticket[]>("/api/" + idProject + "/tickets")
  }

  getTicketByProjectIdAndTicketId(idProject: number, idTicket: number): Observable<Ticket>{
    return this.httpClient.get<Ticket>("/api/" + idProject + "/" + idTicket + "/ticket")
  } 

  saveNewTicketForProject(monNouveauTicket: Ticket, idProject: number): void {
    this.httpClient.post<Ticket>("/api/" + idProject + "/ticket", monNouveauTicket).subscribe()
  }

  updateMyTicket(monTicketUpdate: Ticket, idProject: number, idTicket: number){
    this.httpClient.put<Ticket>("/api/" + idProject + "/" + idTicket + "/updateTicket", monTicketUpdate).subscribe()
  }

  deleteTicket(idProject: number, idTicket: number){
    this.httpClient.delete<Ticket>("/api/" +idProject + "/" + idTicket + "/deleteTicket").subscribe()
  }

}
