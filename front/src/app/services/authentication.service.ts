import { Injectable } from '@angular/core';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  
  private readonly LOCAL_STORAGE_KEY = 'currentUser'

  login(user: User) {
    localStorage.setItem(this.LOCAL_STORAGE_KEY, JSON.stringify(user));
  }

  logout(): void {
    localStorage.removeItem(this.LOCAL_STORAGE_KEY);
  }

  getCurrentUserBasicAuthentication(): string | undefined {
    const currentUserPlain = localStorage.getItem(this.LOCAL_STORAGE_KEY)
    if (currentUserPlain) {
      const currentUser = JSON.parse(currentUserPlain)
      return "Basic " + btoa(currentUser.username + ":" + currentUser.password);
    } else {
      return undefined;
    }
  }

  getCurrentUser(): User | undefined {
    const currentUserPlain = localStorage.getItem(this.LOCAL_STORAGE_KEY)
    if (currentUserPlain) {
      const currentUser: User = JSON.parse(currentUserPlain)
      return currentUser;
    } else {
      return undefined;
    }
  }
}
