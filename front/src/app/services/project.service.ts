import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Project } from '../model/project';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private API_CALL = "/api/projects"
  private API_CALL_POST = "/api/project"

  constructor(private httpClient: HttpClient) { }
  

  getProjects(): Observable<Project[]>{
    return this.httpClient.get<Project[]>(this.API_CALL)
  }

  getProjectById(idProject: number): Observable<Project>{
    return this.httpClient.get<Project>(this.API_CALL_POST + "/" + idProject)
  }

  saveNewProject(monNouveauProject: Project): void{
    this.httpClient.post<Project>(this.API_CALL_POST, monNouveauProject).subscribe()
  }

  updateProject(monProjectUpdate: Project ,idProject: number){
    this.httpClient.put<Project>("/api/" + idProject + "/" + "/updateProject", monProjectUpdate).subscribe()
  }

  deleteProject(idProject: number){
    this.httpClient.delete<Project>("/api/" + idProject + "/deleteProject").subscribe()
  }

}
