import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { ProjectComponent } from './component/project/project.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { CreateProjectComponent } from './pages/create-project/create-project.component';
import { CreateTicketComponent } from './pages/create-ticket/create-ticket.component';
import { UpdateTicketComponent } from './pages/update-ticket/update-ticket.component';
import { UpdateProjectComponent } from './pages/update-project/update-project.component';
import { DetailTicketComponent } from './pages/detail-ticket/detail-ticket.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' }, // redirige les chemins '/s' vers la page Home '/home'
  { path: 'home', component: HomeComponent },
  { path: 'project', component: ProjectComponent },
  { path: 'login', component: LoginComponent },
  { path: 'detailTicket/:idProject/:idTicket', component: DetailTicketComponent },
  { path: 'addProject', component: CreateProjectComponent },
  { path: 'addTicket/:id', component: CreateTicketComponent },
  { path: 'updateTicket/:idProject/:idTicket', component: UpdateTicketComponent },
  { path: 'updateProject/:idProject', component: UpdateProjectComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
