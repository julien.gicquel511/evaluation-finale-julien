import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ticket } from 'src/app/model/ticket';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { TicketService } from 'src/app/services/ticket.service';

@Component({
  selector: 'app-detail-ticket',
  templateUrl: './detail-ticket.component.html',
  styleUrls: ['./detail-ticket.component.css']
})
export class DetailTicketComponent implements OnInit{

  idProject!: number
  idTicket!: number
  monTicket!: Ticket

  constructor(private ticketService: TicketService, private router: Router, private route: ActivatedRoute, private auth: AuthenticationService){
    this.auth = auth
    this.ticketService = ticketService
    this.router = router
  }

  ngOnInit(): void {
    this.idProject = Number(this.route.snapshot.paramMap.get('idProject'));
    this.idTicket = Number(this.route.snapshot.paramMap.get('idTicket'));
    this.ticketService.getTicketByProjectIdAndTicketId(this.idProject, this.idTicket).subscribe(data => {
      this.monTicket = data
    })
  }

}
