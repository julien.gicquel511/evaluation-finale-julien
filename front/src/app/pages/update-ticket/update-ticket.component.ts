import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Status } from 'src/app/model/status';
import { Ticket } from 'src/app/model/ticket';
import { User } from 'src/app/model/user';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { TicketService } from 'src/app/services/ticket.service';

@Component({
  selector: 'app-update-ticket',
  templateUrl: './update-ticket.component.html',
  styleUrls: ['./update-ticket.component.css']
})
export class UpdateTicketComponent {
  monTicket!: Ticket
  @ViewChild(NgForm) myForm!: NgForm;
  formData: {title: string, contenu: string, status: string} = { title: '', contenu: '', status: ""};
  monTicketUpdate!: Ticket
  idProject!: number
  idTicket!: number

  constructor(private ticketService: TicketService, private router: Router, private route: ActivatedRoute, private auth: AuthenticationService){
    this.auth = auth
    this.ticketService = ticketService
    this.router = router
    if(localStorage.length === 0){
      router.navigate(['home'])
    }
  }
  ngOnInit(): void {
    this.idProject = Number(this.route.snapshot.paramMap.get('idProject'));
    this.idTicket = Number(this.route.snapshot.paramMap.get('idTicket'));
    console.log(this.idProject, this.idTicket)
    this.ticketService.getTicketByProjectIdAndTicketId(this.idProject, this.idTicket).subscribe(data => {
      this.monTicket = data
      this.formData.title = data.title
      this.formData.contenu = data.contenu
      this.formData.status = data.status
    })
  }

  user?: User = this.auth.getCurrentUser()

  update(){
    if(this.myForm.valid){
      if(this.user){
        this.monTicketUpdate = new Ticket(this.formData.title, this.formData.contenu , (<any>Status)[this.formData.status])
        this.ticketService.updateMyTicket(this.monTicketUpdate, this.idProject, this.idTicket)
        this.router.navigateByUrl('/home')
      }
    }
  }

  delete(){
    this.ticketService.deleteTicket(this.idProject, this.idTicket)
    this.router.navigateByUrl('/home')
  }
}
