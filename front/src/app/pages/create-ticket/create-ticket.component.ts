import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Status } from 'src/app/model/status';
import { Ticket } from 'src/app/model/ticket';
import { User } from 'src/app/model/user';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { TicketService } from 'src/app/services/ticket.service';

@Component({
  selector: 'app-create-ticket',
  templateUrl: './create-ticket.component.html',
  styleUrls: ['./create-ticket.component.css']
})
export class CreateTicketComponent implements OnInit{
  @ViewChild(NgForm) myForm!: NgForm;
  formData: {title: string, contenu: string} = { title: '', contenu: ''};
  monNouveauTicket!: Ticket
  idProject!: number

  constructor(private ticketService: TicketService, private router: Router, private route: ActivatedRoute, private auth: AuthenticationService){
    this.auth = auth
    this.ticketService = ticketService
    this.router = router
    if(localStorage.length === 0){
      router.navigate(['home'])
    }
  }
  ngOnInit(): void {
    this.idProject = Number(this.route.snapshot.paramMap.get('id'));
    console.log(this.idProject)
  }

  user?: User = this.auth.getCurrentUser()

  save(){
    if(this.myForm.valid){
      if(this.user){
        this.monNouveauTicket = new Ticket(this.formData.title, this.formData.contenu, Status.TODO)
        this.ticketService.saveNewTicketForProject(this.monNouveauTicket, this.idProject)
        this.router.navigateByUrl('/home')
      }
    }
  }
}
