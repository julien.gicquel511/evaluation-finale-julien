import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Project } from '../../model/project';
import { User } from '../../model/user';
import { Ticket } from '../../model/ticket';
import { ProjectService } from '../../services/project.service';
import { Route, Router } from '@angular/router';
import { HttpContext, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.css']
})
export class CreateProjectComponent {
  @ViewChild(NgForm) myForm!: NgForm;
  formData: {name: string, description: string} = { name: '', description: ''};
  monNouveauProject!: Project

  constructor(private projectService: ProjectService, private router: Router, private auth: AuthenticationService){
    this.auth = auth
    this.projectService = projectService
    this.router = router
    if(localStorage.length === 0){
      router.navigate(['home'])
    }
  }

  user?: User = this.auth.getCurrentUser()


  save(){
    if(this.myForm.valid){
      if(this.user){
        this.monNouveauProject = new Project(this.formData.name, this.formData.description, this.user)
        this.projectService.saveNewProject(this.monNouveauProject)
        this.router.navigateByUrl('/home')
      }
    }
  }
}
