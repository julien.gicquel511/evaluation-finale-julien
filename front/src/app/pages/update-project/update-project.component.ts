import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Project } from 'src/app/model/project';
import { User } from 'src/app/model/user';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-update-project',
  templateUrl: './update-project.component.html',
  styleUrls: ['./update-project.component.css']
})
export class UpdateProjectComponent {
  monProject!: Project
  @ViewChild(NgForm) myForm!: NgForm;
  formData: {name: string, descrpition: string} = { name: '', descrpition: ''};
  monProjectUpdate!: Project
  idProject!: number

  constructor(private projectService: ProjectService, private router: Router, private route: ActivatedRoute, private auth: AuthenticationService){
    this.auth = auth
    this.projectService = projectService
    this.router = router
    if(localStorage.length === 0){
      router.navigate(['home'])
    }
  }
  ngOnInit(): void {
    this.idProject = Number(this.route.snapshot.paramMap.get('idProject'));
    this.projectService.getProjectById(this.idProject).subscribe(data => {
      this.monProject = data
      this.formData.name = data.name
      this.formData.descrpition = data.description
    })
  }

  user?: User = this.auth.getCurrentUser()

  update(){
    if(this.myForm.valid){
      if(this.user){
        this.monProjectUpdate = new Project(this.formData.name, this.formData.descrpition, this.monProject.auteur, this.monProject.tickets)
        this.projectService.updateProject(this.monProjectUpdate, this.idProject)
        this.router.navigateByUrl('/home')
      }
    }
  }

  delete(){
    this.projectService.deleteProject(this.idProject);
    this.router.navigateByUrl('/home')
  }
}
