import { AfterViewInit, Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { User } from '../../model/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements AfterViewInit, OnInit{
  localUser!: number

  constructor(private auth: AuthenticationService, private router: Router){
    this.auth = auth
    this.router = router
  }
  ngOnInit(): void {
    if(localStorage.length > 0){
      this.localUser = 1
    } else {
      this.localUser = 0
    }
  }
  ngAfterViewInit(): void {
    this.ngOnInit()
  }

  logout(){
    this.localUser = 0
    this.auth.logout()
  }

}
