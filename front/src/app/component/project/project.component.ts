import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { Observable, map } from 'rxjs';
import { Project } from '../../model/project';
import { tick } from '@angular/core/testing';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit, AfterViewInit{
  projects!: Observable<Project[]>

  constructor(private projectService: ProjectService){
    this.projectService = projectService
  }
  ngAfterViewInit(): void {
    this.ngOnInit()
  }

  ngOnInit(): void {
    this.listAllProjects()
  }

  listAllProjects(): Observable<Project[]>{
    this.projects = this.projectService.getProjects().pipe(map(data => data.sort((a: Project, b: Project) => new Date(b.dateCreation).getTime() - new Date(a.dateCreation).getTime())))

    this.projects.subscribe(data => {
      data.forEach(element => {
        console.log(element)
      })
    })
    return this.projects
  }

}
