import { Component, Input, OnInit } from '@angular/core'

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css'],
})
export class ButtonComponent implements OnInit {
  @Input('type')
  public buttonType: 'button' | 'submit' = 'button'

  @Input()
  public color: 'primary' | 'danger' | 'secondary' | 'white' = 'primary'

  @Input()
  public mode: 'fill' | 'ghost' = 'fill'

  @Input()
  public icon: boolean = false

  @Input()
  public href?: string | any[] | null | undefined
  
  @Input()
  public disable?: null | boolean = false

  constructor() {}

  get isLink() {
    return !!this.href
  }

  get classes() {
    return `button is-${this.color}${this.icon ? ' is-icon' : ''} button--${
      this.mode
    }`
  }

  ngOnInit(): void {}
}
