import { HtmlParser } from "@angular/compiler"
import { Status } from "./status"

export class Ticket {
    id!: number
    title: string
    contenu: string
    dateCreation: Date
    status!: Status

    constructor(title: string, contenu: string, status: Status){
        this.title = title
        this.contenu = contenu
        this.dateCreation = new Date()
        this.status = status
    }
}
