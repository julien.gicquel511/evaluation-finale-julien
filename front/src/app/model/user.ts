import { provideImageKitLoader } from "@angular/common"
import { Project } from "./project"

export class User {
    id?:number
    nom?: string
    prenom?: string
    email?: string
    username: string
    password: string
    active?: boolean
    roles?: string
    projectsCreated?: Project[]
    
    constructor(id: number, nom: string, prenom: string, email: string, username: string, password: string, active: boolean, roles: string, projectsCreated: Project[]){
        this.id = id
        this.nom = nom
        this.prenom = prenom
        this.email = email
        this.username = username
        this.password = password
        this.active = active
        this.roles = roles
        this.projectsCreated = projectsCreated
    }
}
