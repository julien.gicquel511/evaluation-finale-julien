import { Ticket } from "./ticket"
import { User } from "./user"

const DATE_TIME_FORMAT = 'YYYY-MM-DD';
export class Project {

    id!: number
    name: string
    description: string
    dateCreation: Date
    auteur: User
    tickets?: Ticket[]

    constructor(name: string, descrpition: string, auteur: User, tickets?: Ticket[]){
        this.name = name
        this.description = descrpition
        this.dateCreation = new Date()
        this.auteur = auteur
        this.tickets = tickets
    }
}
