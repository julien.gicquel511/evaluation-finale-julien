package zenika.evaluation.back.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import zenika.evaluation.back.model.Status;

import java.sql.Date;

@Getter
@Setter
@NoArgsConstructor
public class TicketDto {
    private Integer id;
    private String title;
    private String contenu;
    private Date dateCreation;
    private Status status = Status.TODO;
}
