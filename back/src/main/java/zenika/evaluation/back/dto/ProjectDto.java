package zenika.evaluation.back.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import zenika.evaluation.back.model.Ticket;
import zenika.evaluation.back.model.User;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ProjectDto {
    private Integer id;
    private String name;
    private String description;
    private Date dateCreation;
    private List<TicketDto> tickets = new ArrayList<>();
    private UserDto auteur;
}
