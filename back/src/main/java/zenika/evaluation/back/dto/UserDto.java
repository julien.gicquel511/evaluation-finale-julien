package zenika.evaluation.back.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import zenika.evaluation.back.model.Project;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class UserDto {
    private int id;
    private String nom;
    private String prenom;
    private String email;
    private String username;
    private String password;
}
