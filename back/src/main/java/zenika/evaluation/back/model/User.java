package zenika.evaluation.back.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "\"User\"")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String nom;
    private String prenom;
    private String email;
    private String username;
    private String password;
    private boolean active;
    private String roles;

    @OneToMany(cascade = {CascadeType.REMOVE})
    private List<Project> projectsCreated = new ArrayList<>();
}
