package zenika.evaluation.back.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "project")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name", length = 255)
    private String name;

    @Column(name = "description", length = 500)
    private String descrpition;

    @Column(name = "date_creation")
    private Date dateCreation = Date.valueOf(new Date(1).toLocalDate());;

    @OneToMany(cascade = {CascadeType.ALL})
    private List<Ticket> ticketList = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User auteur;

}
