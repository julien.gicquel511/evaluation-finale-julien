package zenika.evaluation.back.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.sql.Date;

@Entity
@Getter
@Setter
@Table(name = "ticket")
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project myProject;

    @Column(name = "titre", length = 50)
    private String title;

    @Column(name = "contenu", length = 1000)
    private String contenu;

    @Column(name = "date_creation")
    private Date dateCreation = Date.valueOf(new Date(1).toLocalDate());

    @Column(name = "status")
    private Status status = Status.TODO;

}
