package zenika.evaluation.back.model;

public enum Status {
    TODO,
    INPROGRESS,
    DONE;
}
