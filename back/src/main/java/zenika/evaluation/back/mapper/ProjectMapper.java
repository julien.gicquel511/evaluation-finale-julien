package zenika.evaluation.back.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zenika.evaluation.back.dao.ProjectDao;
import zenika.evaluation.back.dto.ProjectDto;
import zenika.evaluation.back.dto.TicketDto;
import zenika.evaluation.back.model.Project;
import zenika.evaluation.back.model.Ticket;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Component
public class ProjectMapper {
    @Autowired ProjectDao projectDao;
    @Autowired UserMapper userMapper;
    @Autowired TicketMapper ticketMapper;

    public Project projectDtoToProject(ProjectDto projectDto){
        Project project = new Project();
        project.setId(projectDto.getId());
        project.setName(projectDto.getName());
        project.setDescrpition(projectDto.getDescription());
        project.setDateCreation(projectDto.getDateCreation());
        List<Ticket> allTickets = new ArrayList<>();
        for(TicketDto ticketDto : projectDto.getTickets()){
            allTickets.add(ticketMapper.ticketDtoToTicket(ticketDto));
        }
        project.setTicketList(allTickets);

        return project;
    }
    public ProjectDto projectToProjectDto(Project project){
        ProjectDto projectDto = new ProjectDto();
        projectDto.setId(project.getId());
        projectDto.setName(project.getName());
        projectDto.setDescription(project.getDescrpition());
        projectDto.setDateCreation(project.getDateCreation());
        projectDto.setAuteur(userMapper.userToUserDto(project.getAuteur()));
        List<TicketDto> ticketDtos = new ArrayList<>();
        for(Ticket ticket : project.getTicketList()){
            ticketDtos.add(ticketMapper.ticketToTicketDto(ticket));
        }
        projectDto.setTickets(ticketDtos);

        return projectDto;
    }
}
