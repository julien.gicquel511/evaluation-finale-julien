package zenika.evaluation.back.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zenika.evaluation.back.dao.ProjectDao;
import zenika.evaluation.back.dao.TicketDao;
import zenika.evaluation.back.dto.ProjectDto;
import zenika.evaluation.back.dto.TicketDto;
import zenika.evaluation.back.model.Project;
import zenika.evaluation.back.model.Ticket;

import java.util.ArrayList;
import java.util.List;

@Component
public class TicketMapper {
    @Autowired TicketDao ticketDao;
    @Autowired ProjectDao projectDao;

    public Ticket ticketDtoToTicket(TicketDto ticketDto){
        Ticket ticket = new Ticket();
        ticket.setId(ticketDto.getId());
        ticket.setTitle(ticketDto.getTitle());
        ticket.setContenu(ticketDto.getContenu());
        ticket.setDateCreation(ticketDto.getDateCreation());
        ticket.setStatus(ticketDto.getStatus());
        return ticket;
    }
    public TicketDto ticketToTicketDto(Ticket ticket){
        TicketDto ticketDto = new TicketDto();
        ticketDto.setId(ticket.getId());
        ticketDto.setTitle(ticket.getTitle());
        ticketDto.setContenu(ticket.getContenu());
        ticketDto.setDateCreation(ticket.getDateCreation());
        ticketDto.setStatus(ticket.getStatus());
        return ticketDto;
    }
}
