package zenika.evaluation.back.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zenika.evaluation.back.dao.ProjectDao;
import zenika.evaluation.back.dao.UserDao;
import zenika.evaluation.back.dto.TicketDto;
import zenika.evaluation.back.dto.UserDto;
import zenika.evaluation.back.model.Project;
import zenika.evaluation.back.model.Ticket;
import zenika.evaluation.back.model.User;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserMapper {

    @Autowired UserDao userDao;
    @Autowired ProjectDao projectDao;
    public User userDtoToUser(UserDto userDto){
        User user = new User();
        user.setId(userDto.getId());
        user.setNom(userDto.getNom());
        user.setPrenom(userDto.getPrenom());
        user.setEmail(userDto.getEmail());
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        List<Project> allMyProject = new ArrayList<>();
        user.setProjectsCreated(allMyProject);
        return user;
    }
    public UserDto userToUserDto(User user){
        UserDto userDto = new UserDto();
        userDto.setNom(user.getNom());
        userDto.setPrenom(user.getPrenom());
        userDto.setEmail(user.getEmail());
        userDto.setUsername(user.getUsername());
        userDto.setPassword(user.getPassword());
        return userDto;
    }

}
