package zenika.evaluation.back.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import zenika.evaluation.back.dao.UserDao;
import zenika.evaluation.back.model.User;

import java.util.Optional;

@Service
public class MyUserDetailsService  implements UserDetailsService {
    @Autowired UserDao userDao;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userDao.findByUsername(username);
        user.orElseThrow(() -> new UsernameNotFoundException("User " + username + " not found"));
        return user.map(MyUserDetails::new).get();
    }
}
