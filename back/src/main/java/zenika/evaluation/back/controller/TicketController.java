package zenika.evaluation.back.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import zenika.evaluation.back.dto.TicketDto;
import zenika.evaluation.back.service.TicketService;

import java.util.List;

@RestController
@RequestMapping("/api")
public class TicketController {
    @Autowired TicketService ticketService;

    @GetMapping("{idProject}/tickets")
    public List<TicketDto> getTicketsforProject(@PathVariable int idProject){
        return ticketService.getTicketsByProject(idProject);
    }

    @GetMapping("{idProject}/{idTicket}/ticket")
    public TicketDto getTicketforProject(@PathVariable("idProject") int idProject, @PathVariable("idTicket") int idTicket){
        return ticketService.getTicketByProjectIdAndTicketId(idProject, idTicket);
    }

    @PreAuthorize("hasRole('UTILISATEUR')")
    @PostMapping("{idProject}/ticket")
    public void createTicket(@PathVariable int idProject, @RequestBody TicketDto ticketDto){
        ticketService.createTicket(ticketDto, idProject);
    }
    @PreAuthorize("hasRole('UTILISATEUR')")
    @PutMapping("{idProject}/{idTicket}/updateTicket")
    public void updateTicket(@PathVariable int idProject, @PathVariable int idTicket, @RequestBody TicketDto ticketDto){
        ticketService.updateTicket(idProject, idTicket, ticketDto);
    }

    @PreAuthorize("hasRole('UTILISATEUR')")
    @DeleteMapping("{idProject}/{idTicket}/deleteTicket")
    public void deleteProject(@PathVariable int idProject, @PathVariable int idTicket){
        ticketService.deleteTicket(idProject, idTicket);
    }
}
