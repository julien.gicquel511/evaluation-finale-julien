package zenika.evaluation.back.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import zenika.evaluation.back.dto.ProjectDto;
import zenika.evaluation.back.dto.TicketDto;
import zenika.evaluation.back.model.Project;
import zenika.evaluation.back.service.ProjectService;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ProjectController {

    @Autowired ProjectService projectService;
    @GetMapping("/projects")
    public List<ProjectDto> getProjects(){
        return projectService.getProjects();
    }

    @GetMapping("/project/{id}")
    public ProjectDto getProject(@PathVariable int id){
        return projectService.getProject(id);
    }

    @PreAuthorize("hasRole('UTILISATEUR')")
    @PostMapping("/project")
    public void createProject(Principal principal, @RequestBody ProjectDto projectDto){
        projectService.createProject(principal, projectDto);
    }

    @PreAuthorize("hasRole('UTILISATEUR')")
    @PutMapping("{idProject}/updateProject")
    public void updateTicket(@PathVariable int idProject ,@RequestBody ProjectDto projectDto){
        projectService.updateProject(idProject, projectDto);
    }

    @PreAuthorize("hasRole('UTILISATEUR')")
    @DeleteMapping("{idProject}/deleteProject")
    public void deleteProject(@PathVariable int idProject){
        projectService.deleteProject(idProject);
    }

}
