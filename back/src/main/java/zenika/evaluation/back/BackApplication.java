package zenika.evaluation.back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import zenika.evaluation.back.dao.UserDao;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = UserDao.class)
public class BackApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackApplication.class, args);
	}

}
