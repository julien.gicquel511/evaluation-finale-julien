package zenika.evaluation.back.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import zenika.evaluation.back.dao.ProjectDao;
import zenika.evaluation.back.dao.UserDao;
import zenika.evaluation.back.dto.ProjectDto;
import zenika.evaluation.back.dto.UserDto;
import zenika.evaluation.back.mapper.ProjectMapper;
import zenika.evaluation.back.mapper.UserMapper;
import zenika.evaluation.back.model.Project;
import zenika.evaluation.back.model.Ticket;
import zenika.evaluation.back.model.User;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
@Service
public class ProjectService {
    @Autowired ProjectDao projectDao;
    @Autowired ProjectMapper projectMapper;
    @Autowired UserDao userDao;
    @Autowired UserMapper userMapper;
    @Autowired TicketService ticketService;

    public List<ProjectDto> getProjects(){
        List<Project> allProjects = projectDao.findAll();
        List<ProjectDto> allProjectsDto = new ArrayList<>();
        for(Project project : allProjects){
            System.out.println(project.getName());
            allProjectsDto.add(projectMapper.projectToProjectDto(project));
        }
        allProjectsDto.sort(Comparator.comparing(ProjectDto::getDateCreation));
        return allProjectsDto;
    }

    public void createProject(Principal principal, ProjectDto projectDto){
        if(userDao.findByUsername(principal.getName()).isPresent()){
            User myUser = userDao.findByUsername(principal.getName()).get();
            Project myProject = projectMapper.projectDtoToProject(projectDto);
            myProject.setAuteur(myUser);
            myUser.getProjectsCreated().add(myProject);

            System.out.println(myProject.getName());
            System.out.println(myProject.getAuteur().getUsername());

            projectDao.save(myProject);
        }
    }

    public ResponseEntity.HeadersBuilder<?> updateProject(int idProject, ProjectDto projectDto) {
        if(projectDao.findById(idProject).isPresent()){
            Project myProject = projectDao.findById(idProject).get();
            myProject.setName(projectDto.getName());
            myProject.setDescrpition(projectDto.getDescription());
            projectDao.save(myProject);
        } else {
            return ResponseEntity.notFound();
        }
        return null;
    }

    public ProjectDto getProject(int idProject) {
        return projectMapper.projectToProjectDto(projectDao.findById(idProject).get());
    }

    public void deleteProject(int idProject) {
        if(projectDao.findById(idProject).isPresent()){
            Project monProject = projectDao.findById(idProject).get();
            for(Ticket ticket : monProject.getTicketList()){
                monProject.getTicketList().remove(ticket);
                ticketService.deleteTicket(idProject, ticket.getId());
            }
            monProject.getAuteur().getProjectsCreated().remove(monProject);
            projectDao.delete(monProject);
        }
    }
}
