package zenika.evaluation.back.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import zenika.evaluation.back.dao.ProjectDao;
import zenika.evaluation.back.dao.TicketDao;
import zenika.evaluation.back.dao.UserDao;
import zenika.evaluation.back.dto.TicketDto;
import zenika.evaluation.back.mapper.TicketMapper;
import zenika.evaluation.back.model.Project;
import zenika.evaluation.back.model.Ticket;

import java.util.ArrayList;
import java.util.List;

@Service
public class TicketService {
    @Autowired TicketDao ticketDao;
    @Autowired TicketMapper ticketMapper;
    @Autowired ProjectDao projectDao;
    @Autowired
    UserDao userDao;

    public List<TicketDto> getTicketsByProject(Integer idProject){
        List<TicketDto> allMyTicketsDto = new ArrayList<>();
        if(projectDao.findById(idProject).isPresent()){
            Project myProject = projectDao.findById(idProject).get();
            for(Ticket ticket : myProject.getTicketList()){
                allMyTicketsDto.add(ticketMapper.ticketToTicketDto(ticket));
            }
        }
        return allMyTicketsDto;
    }

    public void createTicket(TicketDto ticketDto ,Integer idProject){
        Project myProject = projectDao.findById(idProject).get();
        if(projectDao.existsById(idProject)){
            Ticket monTicket = ticketMapper.ticketDtoToTicket(ticketDto);
            monTicket.setMyProject(projectDao.findById(idProject).get());
            ticketDao.save(monTicket);
            updateTicketListOfMyProject(idProject);
        } else {
            ResponseEntity.notFound();
        }
    }

    public ResponseEntity.HeadersBuilder<?> updateTicket(int idProject, int idTicket, TicketDto ticketDto) {
        if(projectDao.findById(idProject).isPresent()){
            if(ticketDao.findById(idTicket).isPresent()){
                Ticket myTicket = ticketDao.findTicketByIdAndMyProject_Id(idTicket, idProject);
                myTicket.setTitle(ticketDto.getTitle());
                myTicket.setContenu(ticketDto.getContenu());
                myTicket.setStatus(ticketDto.getStatus());
                ticketDao.save(myTicket);
                updateTicketListOfMyProject(idProject);
            } else {
                return ResponseEntity.notFound();
            }
        } else {
            return ResponseEntity.notFound();
        }
        return null;
    }

    //Use for implement the List of Ticket when a ticket is created/modify/delete
    public void updateTicketListOfMyProject(int idProject){
        Project myProject = projectDao.findById(idProject).get();
        List<Ticket> allMyTicketForProject = new ArrayList<>(ticketDao.findByMyProject_Id(idProject));
        myProject.setTicketList(allMyTicketForProject);
        projectDao.save(myProject);
    }

    public TicketDto getTicketByProjectIdAndTicketId(int idProject, int idTicket) {
        Ticket ticket = ticketDao.findTicketByIdAndMyProject_Id(idTicket, idProject);
        return ticketMapper.ticketToTicketDto(ticket);
    }

    public void deleteTicket(int idProject, int idTicket){
        Ticket ticket = ticketDao.findTicketByIdAndMyProject_Id(idTicket, idProject);
        ticket.getMyProject().getTicketList().remove(ticket);

        ticketDao.delete(ticket);
    }
}
