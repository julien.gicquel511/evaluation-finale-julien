package zenika.evaluation.back.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import zenika.evaluation.back.model.User;

import java.util.Optional;

public interface UserDao extends JpaRepository<User, Integer> {
    Optional<User> findByUsername(String userName);

}
