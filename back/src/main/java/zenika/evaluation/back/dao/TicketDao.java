package zenika.evaluation.back.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import zenika.evaluation.back.model.Ticket;

import java.util.List;

public interface TicketDao extends JpaRepository<Ticket, Integer> {
    List<Ticket> findByMyProject_Id(int idProject);
    Ticket findTicketByIdAndMyProject_Id(int idTicket, int idProject);
}
