package zenika.evaluation.back.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import zenika.evaluation.back.model.Project;
import zenika.evaluation.back.model.Ticket;

public interface ProjectDao extends JpaRepository<Project, Integer> {

}
